# Templates
### Templates for different kinds of files.
#### Currently there are these kinds of files:
  * C source and headers
  * C++ source and headers
  * DOS assembly
  * GAS
  * Go
  * GTK+:
    * C
    * Glade designer
    * Python 3
  * Haskell
  * LICENSE (GPLv3)
  * Makefiles for:
   * C
   * C++
   * Python 2 and 3 modules(need SWIG)
   * shared libraries
   * Vala(including GTK+)
  * NASM
  * ODT
  * Python 2
  * Python 3
  * README in Markdown
  * Ruby
  * Shell
  * Text
  * Vala

#### You are welcome to submit pull requests to add your templates!
