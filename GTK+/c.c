#include <gtk/gtk.h>

#ifndef UI_FILE
#define UI_FILE "ui.glade"
#endif

void callback (GtkWidget *widget, gpointer data)
{
	/* code */
}

int main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);

	GtkBuilder* builder = gtk_builder_new_from_file (UI_FILE);

	//GtkWidget *window = (GtkWidget *)gtk_builder_get_object (builder, "window1");

	gtk_builder_connect_signals (builder, NULL);
	//gtk_widget_show_all (window);
	gtk_main ();

	return 0;
}
