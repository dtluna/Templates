.model      small  

CLOSE_PROGRAM_FUNC = 4c00h

init_data macro
	mov ax, data            ;\
    mov ds, ax              ; > load data segment
    mov es, ax              ;/
endm

close_program macro
quit:    
    mov ax, CLOSE_PROGRAM_FUNC           
    int 21h 

.data     
    
.stack

.code
